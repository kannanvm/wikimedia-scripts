import requests, json

# xtools API url -> "https://xtools.wmflabs.org/api/user/simple_editcount/<project_name>/<username>"
API_URL = "https://xtools.wmflabs.org/api/user/simple_editcount"
PROJECT_LIST = ["ml.wikipedia.org","ml.wikisource.org","commons.wikimedia.org","wikidata.org"]

# Using readlines()
userList = open('user_list.txt', 'r')
users = userList.readlines()

userDataString = ''

count = 0
# Strips the newline character
for user in users:
    count += 1
    userDataString = userDataString +'"'+ user.strip()
    for i in range(len(PROJECT_LIST)):
        print(API_URL+"/"+PROJECT_LIST[i]+"/"+user)
        dataRequest = requests.get(API_URL+"/"+PROJECT_LIST[i]+"/"+user)
        projectData = dataRequest.text
        projectDataJSON = json.loads(projectData)
        try:
            userDataString = userDataString + '","' + str(projectDataJSON['live_edit_count'])
        except:
            userDataString = userDataString + '","' + '0'
    userDataString = userDataString + '"\n'
    print(userDataString)

userList.close()

# Saving to a csv file
userData = open("user_data.csv","w")
userData.write(userDataString)
userData.close
